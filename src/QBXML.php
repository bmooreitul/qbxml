<?php

	namespace Itul\QBXML;

	class QBXML{

		public function prepare_string($string, $length = false){

			//$string = str_replace ( array ( '&', '"', "'", '<', '>' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );;
			//$string = htmlspecialchars($string, ENT_XML1, 'UTF-8');
			if($length !== false){
				$string = substr($string, 0, $length);
			}

			return $string;
		}

		public function prepare($string, $length = false){		

			if(is_array($string)){

				if(isset($string['string'])){
					$length = false;
					if(isset($string['length'])){
						$length = $string['length'];
					}
					return $this->prepare($string['string'], $length);
				}
				else{
					foreach($string as $k => $v){
						$string[$k] = $this->prepare($v, $length);
					}
					return $string;
					
				}
			}
			else{
				return $this->prepare_string($string, $length);
			}
		}

		public function convert_to_xml($data){
			$xml_data = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><?qbxml version="13.0"?><QBXML></QBXML>');
			$data = $this->prepare($data);
			$string = $this->array_to_xml($data, $xml_data);
			$dom = new \DOMDocument;
			$dom->preserveWhiteSpace = FALSE;
			$dom->loadXML($string);
			$dom->formatOutput = TRUE;
			return $dom->saveXML();
		}

		public function utf8_for_xml($string){
		    return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
		}	

		public function array_to_xml($data, $xml_data, $last_key = ''){

			foreach($data as $key => $value){
		        if(is_array($value)){
		            if(is_numeric($key)){
		            	$key = $last_key;
		            }
		            if(!isset($value[0])){
		            	$subnode = $xml_data->addChild($key);
		            	$this->array_to_xml($value, $subnode);
		            }
		            else{
		            	$this->array_to_xml($value, $xml_data, $key);
		            }	            
		        }
		        else{
		        	if($last_key != ''){
		        		$key = $last_key;
		        	}
		            $xml_data->addChild("$key",htmlspecialchars("$value", ENT_XML1, 'UTF-8'));
		        }
		    }
		    $xml = $xml_data->asXML();
		    $xml = str_replace('<QBXMLMsgsRq>', '<QBXMLMsgsRq onError="continueOnError">', $xml);
		    return $this->utf8_for_xml(trim($xml));
		}
	}